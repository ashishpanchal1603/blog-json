import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Blog from './components/blog/Blog'
import Content from './components/content/Content'
import Footer from './components/footer/Footer'
import Nav from './components/navbar/Nav'

function App () {
  return (
    <>
    <BrowserRouter>
    <Nav/>
    <Routes>
      <Route path='/' element={<Content/>}/>
      <Route path='/blog' element={<Blog/>}/>
    </Routes>
    <Footer/>
    </BrowserRouter>
    </>
  )
}

export default App
