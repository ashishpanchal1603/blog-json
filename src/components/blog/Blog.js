import React from 'react'
import {
  GrView,
  GrFacebook,
  GrTwitter,
  GrLinkedinOption,
  GrInstagram
} from 'react-icons/gr'
import { BiCommentDetail } from 'react-icons/bi'
import './Blog.css'
import { useLocation } from 'react-router-dom'

function Blog () {
  const value = useLocation()
  console.log('value :>> ', value)

  return (
    <>
      <div className="container">
        <div
          className="image"
          style={{
            backgroundImage: `url("${value.state.value.sImage}")`,
            backgroundSize: 'cover',
            backgroundPosition: 'center'
          }}
        >
          <div className="details">
            <h1>{value.state.value.sTitle}</h1>
            <div className="show-icon">
              <div className="updated">
                <span> SPORT INFO </span> {value.state.value.dUpdatedAt}
              </div>
              <div className="bottom">
              <span className='bottom-icon'> <BiCommentDetail /> {value.state.value.nCommentsCount}</span>

                <span className='bottom-icon'>
                <GrView /> {value.state.value.nViewCounts}
                </span>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="description">
        <ul>
          <li className="icon">
            <GrFacebook />
          </li>
          <li className="icon">
            <GrTwitter />
          </li>
          <li className="icon">
            <GrLinkedinOption />
          </li>
          <li className="icon">
            <GrInstagram />
          </li>
        </ul>
        <h1 className='desDetails'>{value.state.value.sDescription}</h1>
        <div className='read'>
          <h1>ReadList</h1>
          <h4>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            Repudiandae sunt iure non modi incidunt asperiores.
          </h4>
        </div>
      </div>
    </>
  )
}

export default Blog
