import React from 'react'
import DummyData from '../../database/sports-info.json'
import './Context.css'
import { GrView } from 'react-icons/gr'
import { BiCommentDetail } from 'react-icons/bi'
import { AiOutlineArrowDown } from 'react-icons/ai'

import { useNavigate } from 'react-router-dom'
function Content () {
  const navigate = useNavigate()
  const handleClick = (e, value) => {
    console.log('value', value)
    navigate('/blog', { state: { value } })
  }
  return (
    <>
    <div className='content'>
      {DummyData.data.map((value, key) => {
        return (
    <div className="card" key={key} onClick={(e, _id) => handleClick(e, value)}>
      <div className="img-content">
        <img src={value.sImage} alt="" />
      </div>
      <div className="card-content">
        <div><h2> {value.sTitle}</h2></div>
           <br />
        <div><p> {value.sSlug}</p></div>
            <br />
        <div className="bottom-details">
          <div className='updated'>
            <span> SPORT INFO </span> {value.dUpdatedAt}
          </div>
          <div className='bottom'>
           <BiCommentDetail />
            {value.nCommentsCount}
            <GrView /> {value.nViewCounts}
          </div>
        </div>
      </div>
    </div>
        )
      })}
  </div>
        <button className='btn'><span className='load'>Load more</span> <span className='downArrow'><AiOutlineArrowDown/></span> </button>

    </>
  )
}

export default Content
