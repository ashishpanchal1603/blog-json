import React from 'react'
import './Footer.css'
import { GrFacebook, GrTwitter, GrLinkedinOption, GrInstagram } from 'react-icons/gr'
function Footer () {
  return (
    <>
        <footer>
            <ul>
                <li>ABOUT</li>
                <li>CONTACT</li>
                <li>BLOG</li>
                <li>ADVERTISE WITH US</li>
                <li>TERM&CONDITION</li>
                <li>PRIVACY POLICY</li>
            </ul>
            <ul>
                <li className='icon'><GrFacebook/></li>
                <li className='icon'><GrTwitter/></li>
                <li className='icon'><GrLinkedinOption/></li>
                <li className='icon'><GrInstagram/></li>
            </ul>
        </footer>
    </>
  )
}

export default Footer
