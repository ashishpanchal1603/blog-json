import React from 'react'
import './Nav.css'
import { FaSistrix } from 'react-icons/fa'
import { BsPersonCircle } from 'react-icons/bs'
import { SiGoogletranslate } from 'react-icons/si'
function Nav () {
  return (
    <>
        <nav>
            <div>
                <img src="https://www.nicepng.com/png/detail/27-277799_star-sports-star-sports-logo-png.png" className='sportLogo' alt=""/>
            </div>
            <ul className='game'>
                <li><a href="">CRICKET</a></li>
                <li><a href=""> KABADDI </a></li>
                <li><a href=""> SOCCER </a></li>
                <li><a href=""> BASKET BALL </a></li>
                <li> <a href=""> FIELD HOCKEY </a></li>
                <li><a href=""> BADMINTON </a></li>
                <li><a href=""> RACING </a></li>
                <li><a href=""> TENNIS SPORT </a></li>
                <li><a href=""> E-SPORT </a></li>
                <li><a href=""> WWE </a></li>
            </ul>
            <ul className='search'>
                <li className='translate'> <a href=""><SiGoogletranslate/></a></li>
                <li><a href=""> <FaSistrix/></a></li>
                <li className='account'><a href=""> <BsPersonCircle/></a></li>
            </ul>
        </nav>
    </>
  )
}

export default Nav
